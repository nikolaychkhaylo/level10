#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	int totalsize = 20;
    // Allocate memory for an array of strings (arr).
	char ** words = malloc(20 * sizeof(char **));
    
    // Read the dictionary line by line.
	char line[255];
    *size = 0;
    while (fgets(line, 255, in) != NULL)
    {
        if (line[strlen(line)-1] == '\n')
        {
            line[strlen(line)-1] = '\0';
        }

        // Expand array if necessary (realloc).
        if(*size == totalsize - 2)
            {
                totalsize = totalsize + 20;
                words = realloc(words, totalsize * sizeof(char **));
            }

        // Allocate memory for the string (str).
        char * currentwords = malloc(13 * sizeof(char));
	    // Copy each line into the string (use strcpy).
        strcpy(currentwords, line);
	    // Attach the string to the large array (assignment =).
        words[* size] = currentwords;
        *size = *size + 1;
    }
	// Return pointer to the array of strings.
	return words;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}